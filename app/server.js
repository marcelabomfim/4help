const express = require('express');
const next = require('next');
const routes = require('./routes');
const config = require('./config');

const dev = process.env.NODE_ENV !== 'production';
const PORT = process.env.PORT || 3000;

const app = next({ dir: './app', dev });
const handler = routes.getRequestHandler(app, ({ req, res, route, query }) => {
  app.render(req, res, route.page, query);
});

const ifaces = require('os').networkInterfaces();

let address;

Object.keys(ifaces).forEach(addressEnv => {
  // eslint-disable-next-line array-callback-return
  ifaces[addressEnv].filter(details => {
    // eslint-disable-next-line prefer-destructuring
    if (details.family === 'IPv4' && details.internal === false) {
      // eslint-disable-next-line prefer-destructuring
      address = details.address;
    }
  });
});

console.log(config);
app.config = config;
app.prepare().then(() => {
  express()
    .use(handler)
    .listen(PORT, err => {
      if (err) throw err;
      console.log(`> Ready on http://${address}:${PORT}`);
    });
});

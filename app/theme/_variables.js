const colors = {
  background: '#fafafa',
  backgroundDark: '#001529',
  primary: '#1890ff',
  white: '#ffffff',
  black: '#424242',
  gray: '#d9d9d9',
  grayMedium: '#efefef',
};

const spacing = {
  xsmall: '6px',
  small: '12px',
  base: '24px',
  large: '36px',
  xlarge: '48px',
};

const defaultValues = {
  border: `1px solid ${colors.gray}`,
  transition: '0.3s cubic-bezier(0.76, 0.03, 0, 0.65)',
  boxShadow: '0 4px 7px 0 rgba(117, 117, 117, 0.15)',
  boxShadowUp: '0 -3px 7px 0 rgba(117, 117, 117, 0.10)',
};

module.exports = {
  colors,
  spacing,
  defaultValues,
};

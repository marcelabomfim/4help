import { Component } from 'react';

import withRedux from 'next-redux-wrapper';
import { initStore, MainActions } from 'store/redux';

import { CoreLayout } from 'components/_base/CoreLayout';
import { CategoriesList } from 'components/CategoriesList';

class CategoriesPage extends Component {
  static async getInitialProps({ store, isServer }) {
    await store.dispatch(MainActions.getCategories());

    return { isServer };
  }

  render() {
    return (
      <CoreLayout page="Auto Atendimento" subPage="Listar" activeMenu="21" activeSubmenu="2">
        <h1 className="title">Categorias de Auto Atendimento</h1>
        <CategoriesList />
      </CoreLayout>
    );
  }
}

export default withRedux(initStore)(CategoriesPage);

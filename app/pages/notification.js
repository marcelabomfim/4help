import { Component } from 'react';
import PropTypes from 'prop-types';

import withRedux from 'next-redux-wrapper';
import { initStore, MainActions } from 'store/redux';

import { CoreLayout } from 'components/_base/CoreLayout';
import { NotificationForm } from 'components/NotificationForm';

class NotificationPage extends Component {
  static async getInitialProps({ store, isServer, query }) {
    const notificationId = query.id || '';

    if (notificationId !== '') {
      await store.dispatch(MainActions.getNotification(notificationId));
    } else {
      await store.dispatch(MainActions.initialNotification());
    }

    return { isServer, notificationId };
  }

  render() {
    const { notification } = this.props;
    const subPage = notification.id !== '' ? 'Editar' : 'Adicionar';
    const activeMenu = notification.id !== '' ? '' : '24';

    return (
      <CoreLayout page="Auto Atendimento" subPage={subPage} activeMenu={activeMenu} activeSubmenu="3">
        <h1 className="title">Notificações de Auto Atendimento</h1>
        <NotificationForm />
      </CoreLayout>
    );
  }
}

NotificationPage.propTypes = {
  notification: PropTypes.object.isRequired,
};

const mapStateToProps = store => ({
  notification: store.MainStore.notification,
});

export default withRedux(initStore, mapStateToProps)(NotificationPage);

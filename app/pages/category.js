import { Component } from 'react';
import PropTypes from 'prop-types';

import withRedux from 'next-redux-wrapper';
import { initStore, MainActions } from 'store/redux';

import { CoreLayout } from 'components/_base/CoreLayout';
import { CategoryForm } from 'components/CategoryForm';

class CategoryPage extends Component {
  static async getInitialProps({ store, isServer, query }) {
    const categoryId = query.id || '';

    if (categoryId !== '') {
      await store.dispatch(MainActions.getCategory(categoryId));
    } else {
      await store.dispatch(MainActions.initialCategory());
    }

    return { isServer, categoryId };
  }

  render() {
    const { category } = this.props;
    const subPage = category.id !== '' ? 'Editar' : 'Adicionar';
    const activeMenu = category.id !== '' ? '' : '22';

    return (
      <CoreLayout page="Auto Atendimento" subPage={subPage} activeMenu={activeMenu} activeSubmenu="2">
        <h1 className="title">Categorias de Auto Atendimento</h1>
        <CategoryForm />
      </CoreLayout>
    );
  }
}

CategoryPage.propTypes = {
  category: PropTypes.object.isRequired,
};

const mapStateToProps = store => ({
  category: store.MainStore.category,
});

export default withRedux(initStore, mapStateToProps)(CategoryPage);

import { CoreLayout } from 'components/_base/CoreLayout';

const HomePage = () => (
  <CoreLayout>
    <h1 className="title">4Help LojasKD!</h1>
    <p>Gestão de conteúdos de Auto Atendimento</p>
  </CoreLayout>
);

export default HomePage;

import { Component } from 'react';

import withRedux from 'next-redux-wrapper';
import { initStore, MainActions } from 'store/redux';

import { CoreLayout } from 'components/_base/CoreLayout';
import { NotificationsList } from 'components/NotificationsList';

class NotificationsPage extends Component {
  static async getInitialProps({ store, isServer }) {
    await store.dispatch(MainActions.getNotifications());

    return { isServer };
  }

  render() {
    return (
      <CoreLayout page="Auto Atendimento" subPage="Listar" activeMenu="23" activeSubmenu="3">
        <h1 className="title">Notificações de Auto Atendimento</h1>
        <NotificationsList />
      </CoreLayout>
    );
  }
}

export default withRedux(initStore)(NotificationsPage);

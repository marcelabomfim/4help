import fetch from 'isomorphic-unfetch';

export default (url, options = {}) => {
  const defaults = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };

  return fetch(url, {
    ...defaults,
    ...options,
    headers: {
      ...defaults.headers,
      ...(options && options.headers),
    },
  })
    .then(response => {
      if (response.ok) return response;

      const error = new Error(response.statusText);
      error.response = response;
      throw error;
    })
    .then(res => res.json());
};

import ReactGA from 'react-ga';

/**
 * @function initGA
 * @description Initialize Google Analytics module
 */
export const initGA = () => {
  ReactGA.initialize('UA-xxxxxxxxx-1');
  console.info('GA init');
};

/**
 * @function logPageView
 * @description Send Page View log to Google Analytics based on the pathname location
 */
export const logPageView = () => {
  ReactGA.set({ page: window.location.pathname });
  ReactGA.pageview(window.location.pathname);
  console.info(`Logging pageview for ${window.location.pathname}`);
};

/**
 * @function logEvent
 * @description Send Events log to Google Analytics
 *
 * @param {string} category - A top level category for group events. E.g. 'Desktop'
 * @param {string} action - A description of the behaviour. E.g. 'Regionalism Modal'
 * @param {string} label - More precise labelling of the related action. E.g. 'Select Paraná'
 * @param {bool} [nonInteraction=false] - Set true if the event is not triggered by a user interaction
 */
export const logEvent = (category, action, label, nonInteraction = false) => {
  const eventParams = { category, action, label, nonInteraction };

  if (nonInteraction !== false) {
    eventParams.nonInteraction = nonInteraction;
  }

  ReactGA.event(eventParams);
  console.info('Logging event:', eventParams);
};

/**
 * @function logException
 * @description Send Exceptions log to Google Analytics
 *
 * @param {string} description - Description of what happened
 * @param {bool} [fatal=false] - Set to true if it was a fatal exception
 */
export const logException = (description, fatal = false) => {
  const exceptionParams = { description, fatal };

  if (fatal !== false) {
    exceptionParams.fatal = fatal;
  }

  ReactGA.exception(exceptionParams);
  console.info('Logging exception:', exceptionParams);
};

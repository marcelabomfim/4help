import { logException as logExceptionGA } from 'helpers/analytics';

/**
 * @function errorLog
 * @description Receive errors events and redirect this logs to LKD Centralized Logs(@todo) and GA module
 *
 * @param {string} description - Description of what happened
 * @param {bool} [fatal=false] - Set to true if it was a fatal exception
 */
export default (description, fatal = false) => {
  const errorParams = { description, fatal };

  logExceptionGA(errorParams);
};

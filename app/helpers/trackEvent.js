import { logEvent as logEventGA } from 'helpers/analytics';

/**
 * @function trackEvent
 * @description Receive track events and redirect this logs to LKD Centralized Logs(@todo) and GA module
 *
 * @param {string} category - A top level category for group events. E.g. 'Desktop'
 * @param {string} action - A description of the behaviour. E.g. 'Regionalism Modal'
 * @param {string} label - More precise labelling of the related action. E.g. 'Select Paraná'
 * @param {bool} [nonInteraction=false] - Set true if the event is not triggered by a user interaction
 */
export default (category, action, label, nonInteraction = false) => {
  logEventGA(category, action, label, nonInteraction);
};

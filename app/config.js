module.exports = {
  appRoot: process.env.APP_ROOT,
  apiRoot: process.env.API_ROOT,
};

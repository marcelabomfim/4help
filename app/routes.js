// eslint-disable-next-line no-multi-assign
const routes = (module.exports = require('next-routes')());

routes
  .add({ pattern: '/categorias', page: 'categories' })
  .add({ pattern: '/categorias/adicionar', page: 'category' })
  .add({ pattern: '/categorias/editar/:id', page: 'category' })
  .add({ pattern: '/notificacoes', page: 'notifications' })
  .add({ pattern: '/notificacoes/adicionar', page: 'notification' })
  .add({ pattern: '/notificacoes/editar/:id', page: 'notification' });

import { Modal, Table, Icon } from 'components/_base/ANTD';

// https://lojaskd.zendesk.com/agent/admin/ticket_fields
const dataSource1 = [
  { title: 'Tipo', field: 'type', required: true, acceptedValues: 'incident / problem / question / task' },
  { title: 'Status', field: 'status', required: true, acceptedValues: 'new / open / closed / pending / hold / solved' },
  { title: 'Assunto', field: 'subject', required: true, acceptedValues: 'texto' },
  { title: 'Descrição', field: 'description', required: true, acceptedValues: 'texto' },
];

const dataSource2 = [
  { title: 'Código OCR', field: 'ocrCode', cf: '78300127', acceptedValues: 'lista' },
  { title: 'Comentário Interno', field: 'private_comment', acceptedValues: 'texto' },
  { title: 'Comentário Público', field: 'public_comment', acceptedValues: 'texto' },
  { title: 'Financeiro', field: 'financial', cf: '44325088', acceptedValues: 'true / false' },
  { title: 'Forma de Restituição', field: 'refund_form', cf: '40584247', acceptedValues: 'lista' },
  { title: 'Iniciar Cancelamento', field: 'begin_cancellation', cf: '80800647', acceptedValues: 'true / false' },
  { title: 'Observações', field: 'observation', cf: '37324007', acceptedValues: 'texto' },
  { title: 'Status OCR / Status Interno', field: 'ocrStatus', cf: '37245848', acceptedValues: 'lista' },
  { title: 'Jira', field: 'jira', cf: '79730647', acceptedValues: 'texto' },
  { title: 'Produtos', field: 'products', cf: '80334527', acceptedValues: 'um ou mais itens separados por ;' },
  { title: 'Ordem de Compra', field: 'oc', cf: '80561807', acceptedValues: 'números' },
  { title: 'NF', field: 'nf', cf: '37270327', acceptedValues: 'números' },
  { title: 'Número do Pedido', field: 'orderId', cf: '36044788', acceptedValues: 'números' },
  { title: 'Grupo', field: 'group_id', acceptedValues: '' },
  { title: 'Estorno', field: 'reversal', cf: '80486367', acceptedValues: 'true / false' },
  { title: 'Motivo', field: 'reason', cf: '39330627', acceptedValues: 'lista' },
  { title: 'Tratativa', field: 'treatment', cf: '39533908', acceptedValues: 'lista' },
  { title: 'Tags - adição', field: 'tags', acceptedValues: 'um ou mais itens separados por ;' },
  { title: 'Tags - remoção', field: 'remove_tags', acceptedValues: 'um ou mais itens separados por ;' },
];

const byTitle = (a, b) => {
  if (a.title.toUpperCase() < b.title.toUpperCase()) {
    return -1;
  } else if (a.title.toUpperCase() > b.title.toUpperCase()) {
    return 1;
  }

  return 0;
};

const dataSourceCreate = [...dataSource1, ...dataSource2.sort(byTitle)];

const dataSourceInfo = [
  {
    title: 'Status do Ticket',
    field: 'status',
    acceptedValues: (
      <span>
        new, open, closed, pending, hold, solved<br />(um ou mais separados por vírgula)
      </span>
    ),
  },
  {
    title: 'Código OCR',
    field: 'ocr',
    acceptedValues: (
      <span>
        AA999, BB - código completo ou só as letras<br />(um ou mais separados por vírgula)
      </span>
    ),
  },
  { title: 'Tem Jira?', field: 'jira', acceptedValues: 'canceled / none' },
  { title: 'Grupo', field: 'group', acceptedValues: 'ID do grupo' },
  { title: 'Financeiro', field: 'financial', acceptedValues: 'true / false' },
  { title: 'Estorno', field: 'reversal', acceptedValues: 'true / false' },
  {
    title: 'Data de criação - início',
    field: 'start_date',
    acceptedValues: (
      <span>
        data no formato AAAA-MM-DD<br />ex: 2018-01-31
      </span>
    ),
  },
  {
    title: 'Data de criação - fim',
    field: 'end_date',
    acceptedValues: (
      <span>
        data no formato AAAA-MM-DD<br />ex: 2018-01-31
      </span>
    ),
  },
];

const columns = [
  {
    title: 'Título',
    dataIndex: 'title',
    key: 'title',
    sortOrder: 'ascend',
    render: (text, record) =>
      record.required ? (
        <strong>
          <span style={{ color: 'red' }}>*</span> {record.title}
        </strong>
      ) : (
        text
      ),
  },
  { title: 'Campo', dataIndex: 'field', key: 'field' },
  {
    title: 'Valores aceitos',
    dataIndex: 'acceptedValues',
    key: 'acceptedValues',
    render: (text, record) =>
      text === 'lista' ? (
        <a href={`https://lojaskd.zendesk.com/agent/admin/ticket_fields/${record.cf}`} target="_blank">
          Valores nesta lista <Icon type="export" />
        </a>
      ) : (
        text
      ),
  },
];

export const createTicketInfo = () => {
  Modal.info({
    width: 760,
    maskClosable: true,
    title: 'Criação/Atualização de Tickets no Zendesk',
    content: (
      <div>
        <p>Informar os campos e valores para criação ou atualização de tickets no zendesk, no seguinte padrão:</p>
        <p>
          <strong>
            campo1 = valor1<br />campo2 = valor2<br />campo3 = valor3
          </strong>
        </p>
        <br />
        <Table dataSource={dataSourceCreate} columns={columns} pagination={false} bordered />
        <br />
        <p>
          <strong>
            <span style={{ color: 'red' }}>*</span> campos obrigatórios para a criação de tickets
          </strong>
        </p>
        <br />
        <p>
          Para usar um valor que o usuário informou no formulário use <strong>__FORM-x__</strong>, onde x pode ser:
          <br />
          VALOR, PRODUTO, PRODUTOS, ANEXOS, DESCRICAO, ENDERECO, TELEFONE
          <br />
          <br />
          <i>Ex.: private_comment: o usuário informou o valor: R$ __FORM-VALOR__</i>
        </p>
      </div>
    ),
  });
};

export const queryTicketInfo = () => {
  Modal.info({
    width: 700,
    maskClosable: true,
    title: 'Consulta de Tickets no Zendesk',
    content: (
      <div>
        <p>Informar os campos e valores para consulta de tickets no zendesk, no seguinte padrão:</p>
        <p>
          <strong>&campo1=valor1&campo2=valor2&campo3=valor3</strong>
        </p>
        <br />
        <Table dataSource={dataSourceInfo} columns={columns} pagination={false} bordered />
      </div>
    ),
  });
};

const dataSourceVariables = [
  { title: '__NAME__', description: 'Nome do cliente' },
  { title: '__PRODUCTS_LIST__', description: 'Lista de produtos mencionados no campo "produtos" do ticket (ou todos os produtos do pedido caso o campo esteja vazio)' },
  { title: '__CANCELED_PRODUCTS_LIST__', description: 'O mesmo da __PRODUCTS_LIST__, porém além de exibir o nome dos produtos também esconde as informações de entraga do produto' },
  { title: '__CHARGEBACK_VALUE__', description: 'Exibe o valor do estorno' },
  { title: '__COUPON_CODE_TRC__', description: 'Cupom para recompra' },
  { title: '__COUPON_CODE_RET_7__', description: 'Cupom de retenção 7%' },
  { title: '__COUPON_CODE_RET_10__', description: 'Cupom de retenção 10%' },
  { title: '__COUPON_CODE_RET_15__', description: 'Cupom de retenção 15%' },
  { title: '__COUPON_VALUE__', description: 'Valor do cupom gerado' },
  { title: '__CF-123__', description: 'Exibe o valor de um custom field do zendesk, onde 123 é o id do custom field, consulta: https://lojaskd.zendesk.com/agent/admin/ticket_fields ' },
];

const columnsVariables = [{ title: 'Variável', dataIndex: 'title', key: 'title', sortOrder: 'ascend', width: 265 }, { title: 'Descrição', dataIndex: 'description', key: 'description' }];

export const textParamsInfo = () => {
  Modal.info({
    width: 700,
    maskClosable: true,
    title: 'Uso de variáveis no texto',
    content: (
      <div>
        <p>Você pode utilizar as variáveis definidas abaixo no meio do texto, ao renderizar a mensagem para o usuário a variável será substituída pelo valor indicado:</p>
        <p>
          <i>
            Ex.: Olá <strong>__NAME__</strong>!
          </i>
        </p>
        <br />
        <Table dataSource={dataSourceVariables} columns={columnsVariables} pagination={false} bordered />
      </div>
    ),
  });
};

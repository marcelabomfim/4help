import PropTypes from 'prop-types';
import { Table, Button, Popconfirm, message } from 'components/_base/ANTD';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MainActions } from 'store/redux';

import StyledCategoriesList from './CategoriesList.styles';
import { Link } from '../../routes';

const CategoriesList = ({ categories, deleteCategory }) => {
  const confirmDelete = id => {
    const hideLoading = message.loading(`Excluindo registro ${id}`, 0);
    deleteCategory(id).then(() => {
      hideLoading();
      setTimeout(() => message.success(`Registro ${id} excluído`), 600);
    });
  };

  const dataSource = categories.map(category => ({
    key: category.title,
    id: category.id,
    title: category.title,
    order_status: typeof category.order_status !== 'undefined' ? category.order_status.join(', ') : '',
    screensQty: category.screens && category.screens.length,
    status: category.status,
  }));

  const columns = [
    {
      title: 'Categoria',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Pedidos nos status',
      dataIndex: 'order_status',
      key: 'order_status',
    },
    {
      title: 'Qtd. Telas',
      dataIndex: 'screensQty',
      key: 'screensQty',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: text => (text === 'active' ? 'Ativa' : 'Inativo'),
    },
    {
      title: 'Ação',
      key: 'action',
      align: 'center',
      width: 250,
      render: (text, record) => (
        <span>
          <Link route={`/categorias/editar/${record.id}`}>
            <Button icon="edit">Editar</Button>
          </Link>
          <Popconfirm title="Tem certeza que deseja excluir este registro?" onConfirm={() => confirmDelete(record.id)} okText="Sim" cancelText="Não" placement="left">
            <Button type="danger" icon="delete">
              Excluir
            </Button>
          </Popconfirm>
        </span>
      ),
    },
  ];

  return (
    <StyledCategoriesList>
      <Table dataSource={dataSource} columns={columns} bordered />
    </StyledCategoriesList>
  );
};

CategoriesList.propTypes = {
  categories: PropTypes.array.isRequired,
  deleteCategory: PropTypes.func.isRequired,
};

const mapStateToProps = store => ({
  categories: store.MainStore.categories,
});

const mapDispatchToProps = dispatch => ({
  deleteCategory: bindActionCreators(MainActions.deleteCategory, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesList);

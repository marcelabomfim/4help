import styled from 'styled-components';
import theme from 'theme/_variables';

export default styled.div`
  .ant-table-thead > tr > th {
    background-color: ${theme.colors.primary};
    color: ${theme.colors.white};
  }

  .ant-btn.ant-btn-danger {
    margin-left: ${theme.spacing.small};
  }
`;

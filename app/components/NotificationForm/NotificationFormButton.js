import PropTypes from 'prop-types';
import { Alert, Form, Input, Select, Checkbox, Button, Row, Col } from 'components/_base/ANTD';
import { createTicketInfo } from 'components/TicketsInfo';

const NotificationFormButton = ({ button, index, screenIndex, screenType, screensOptions, removeButton, getFieldDecorator, handleActionChange }) => {
  const FormItem = Form.Item;

  const handleClick = e => {
    e.preventDefault();
    e.stopPropagation();
    removeButton(screenIndex, index);
  };

  const fieldPrefix = `notification.screens[${screenIndex}].buttons[${index}]`;

  const formFields = [
    { label: 'Anexos', key: 'attachment' },
    { label: 'Descrição', key: 'description' },
    { label: 'Endereço', key: 'address' },
    { label: 'Produto', key: 'product' },
    { label: 'Produtos', key: 'products' },
    { label: 'Telefone', key: 'phone' },
    { label: 'Valor (R$)', key: 'amount' },
  ];

  return (
    <Col span={12} className="button-item">
      <Alert
        type="info"
        message={
          <div>
            <Button className="button-item__delete" type="danger" shape="circle" icon="close" size="small" title="Remover botão" onClick={handleClick} />
            <Row>
              <Col span={8}>
                <FormItem className="no-margin">
                  {getFieldDecorator(`${fieldPrefix}.style`, {
                    initialValue: button.style !== '' ? button.style : undefined,
                    rules: [{ required: screenType === 'content', message: 'campo obrigatório' }],
                  })(
                    <Select placeholder="Estilo do botão">
                      <Select.Option value="primary">Azul</Select.Option>
                      <Select.Option value="danger">Vermelho</Select.Option>
                      <Select.Option value="success">Verde</Select.Option>
                      <Select.Option value="gray">Cinza</Select.Option>
                      <Select.Option value="secondary">Link</Select.Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col span={16}>
                <FormItem className="no-margin">
                  {getFieldDecorator(`${fieldPrefix}.text`, { initialValue: button.text, rules: [{ required: screenType === 'content', message: 'campo obrigatório' }] })(
                    <Input placeholder="Texto do botão" />
                  )}
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={8}>
                <FormItem className="no-margin">
                  {getFieldDecorator(`${fieldPrefix}.action`, {
                    initialValue: button.action !== '' ? button.action : undefined,
                    rules: [{ required: screenType === 'content', message: 'campo obrigatório' }],
                  })(
                    <Select placeholder="Ação do botão" onChange={handleActionChange}>
                      <Select.Option value="show_screen">Exibir tela</Select.Option>
                      <Select.Option value="link">Link nova aba</Select.Option>
                      <Select.Option value="location">Link na mesma aba</Select.Option>
                      <Select.Option value="form">Abrir formulário</Select.Option>
                      <Select.Option value="rebuy">Refazer pedido</Select.Option>
                    </Select>
                  )}
                </FormItem>
              </Col>
              <Col span={16}>
                {button.action === 'show_screen' && (
                  <FormItem className="no-margin">
                    {getFieldDecorator(`${fieldPrefix}.actionValue`, {
                      initialValue: button.actionValue !== '' ? button.actionValue : undefined,
                      rules: [{ required: screenType === 'content', message: 'campo obrigatório' }],
                    })(
                      <Select placeholder="Escolha uma tela" onChange={handleActionChange}>
                        {screensOptions.map(s => (
                          <Select.Option value={s.key} key={s.key}>
                            {s.key} - {s.label}
                          </Select.Option>
                        ))}
                      </Select>
                    )}
                  </FormItem>
                )}
                {(button.action === 'link' || button.action === 'location') && (
                  <FormItem className="no-margin">
                    {getFieldDecorator(`${fieldPrefix}.actionValue`, {
                      initialValue: button.actionValue,
                      rules: [{ required: screenType === 'content', message: 'campo obrigatório' }],
                    })(<Input placeholder="Informe o link" />)}
                  </FormItem>
                )}
                {button.action === 'form' && (
                  <FormItem className="no-margin">
                    {getFieldDecorator(`${fieldPrefix}.actionValue`, {
                      initialValue: typeof button.actionValue === 'object' ? button.actionValue : [],
                      rules: [{ required: screenType === 'content', message: 'campo obrigatório' }],
                    })(
                      <Select placeholder="Informe os campos visíveis" onChange={handleActionChange} mode="multiple">
                        {formFields.map(f => (
                          <Select.Option value={f.key} key={f.key}>
                            {f.label}
                          </Select.Option>
                        ))}
                      </Select>
                    )}
                  </FormItem>
                )}
                {button.action === 'rebuy' && (
                  <FormItem className="no-margin">
                    {getFieldDecorator(`${fieldPrefix}.actionValue`, {
                      initialValue: '__COUPON_CODE_TRC__',
                      rules: [{ required: screenType === 'content', message: 'campo obrigatório' }],
                    })(<Input readOnly />)}
                  </FormItem>
                )}
                {(button.action === '' || !button.action) && (
                  <FormItem className="no-margin">
                    {getFieldDecorator(`${fieldPrefix}.actionValue`, { initialValue: button.actionValue, rules: [{ required: screenType === 'content', message: 'campo obrigatório' }] })(
                      <Input placeholder="Valor da ação" />
                    )}
                  </FormItem>
                )}
              </Col>
            </Row>
            <FormItem className="no-margin">
              {getFieldDecorator(`${fieldPrefix}.update`, { initialValue: button.update, valuePropName: 'checked' })(
                <Checkbox>
                  Enviar atualização / Criar ticket <Button shape="circle" size="small" icon="question" onClick={createTicketInfo} />
                </Checkbox>
              )}
            </FormItem>
            <FormItem className="no-margin">
              {getFieldDecorator(`${fieldPrefix}.updateValue`, { initialValue: button.updateValue, rules: [{ required: button.update === true }] })(
                <Input.TextArea placeholder={`type = task\nstatus = new\nsubject = Teste\ndescription = Ticket de teste`} autosize={{ minRows: 4, maxRows: 4 }} disabled={!button.update} />
              )}
            </FormItem>
          </div>
        }
      />
    </Col>
  );
};

NotificationFormButton.propTypes = {
  button: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  screenIndex: PropTypes.number.isRequired,
  screenType: PropTypes.string.isRequired,
  screensOptions: PropTypes.array.isRequired,
  removeButton: PropTypes.func.isRequired,
  getFieldDecorator: PropTypes.func.isRequired,
  handleActionChange: PropTypes.func.isRequired,
};

export default NotificationFormButton;

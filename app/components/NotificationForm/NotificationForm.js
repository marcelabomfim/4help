import PropTypes from 'prop-types';
import { Component } from 'react';
import { Form, Input, Button, Radio, Select, Row, Col, Icon, message } from 'components/_base/ANTD';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MainActions } from 'store/redux';

import StyledNotificationForm from './NotificationForm.styles';
import NotificationFormScreen from './NotificationFormScreen';
import NotificationFormButton from './NotificationFormButton';
import { Router } from '../../routes';

const buttonSkeleton = { style: '', text: '', action: '', actionValue: '', update: false, updateValue: '' };
const querySkeleton = { status: '', ocrCode: '', ocrStatus: '' };
const screenSkeleton = { internal_identification: '', title: '', text: '', style: '', buttons: [buttonSkeleton], query: querySkeleton };

class NotificationForm extends Component {
  state = {
    buttonLoading: false,
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll(err => {
      if (err) {
        message.error('Por favor preencha corretamente os campos indicados');
        return false;
      }

      if (this.state.buttonLoading) return false;

      const { notification } = this.props;
      const hideLoading = message.loading(`Salvando registro`, 0);
      this.setState({ buttonLoading: true });

      this.props.saveNotification(notification).then(() => {
        hideLoading();
        this.setState({ buttonLoading: false });
        setTimeout(() => {
          message.success(`Registro salvo com sucesso`);
          Router.pushRoute('/notificacoes');
        }, 600);
      });

      return false;
    });
  };

  handleChange = (e = false) => {
    if (e) e.preventDefault();
    const notification = { ...this.props.notification, ...this.props.form.getFieldValue('notification') };
    this.props.updateNotificationProps(notification);
  };

  addScreen = () => {
    const { notification } = this.props;
    notification.screens.push(screenSkeleton);

    this.props.updateNotificationProps(notification);
    this.props.form.setFieldsValue({ notification });
  };

  removeScreen = index => {
    const { notification } = this.props;
    notification.screens = notification.screens.filter((screen, i) => i !== index);

    this.props.updateNotificationProps(notification);
    this.props.form.setFieldsValue({ notification });
  };

  addButton = screenIndex => {
    const { notification } = this.props;
    const buttons = notification.screens[screenIndex].buttons || [];
    buttons.push(buttonSkeleton);
    notification.screens[screenIndex].buttons = buttons;

    this.props.updateNotificationProps(notification);
    this.props.form.setFieldsValue({ notification });
  };

  removeButton = (screenIndex, index) => {
    const { notification } = this.props;
    notification.screens[screenIndex].buttons = notification.screens[screenIndex].buttons.filter((screen, i) => i !== index);

    this.props.updateNotificationProps(notification);
    this.props.form.setFieldsValue({ notification });
  };

  handleActionChange = () => {
    setTimeout(() => this.handleChange(), 1);
  };

  render() {
    const FormItem = Form.Item;
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    const { notification } = this.props;
    const screensOptions = notification.screens.map((s, i) => ({ key: i, label: s.internal_identification })).filter(s => s.label !== '');

    return (
      <Form onSubmit={this.handleSubmit} onChange={this.handleChange}>
        <StyledNotificationForm>
          <Row gutter={24}>
            <Col span={12}>
              <FormItem label="Título da notificação">
                {getFieldDecorator('notification.title', { initialValue: notification.title, rules: [{ required: true, message: 'campo obrigatório' }] })(<Input />)}
              </FormItem>
            </Col>
            <Col span={6}>
              <FormItem label="Prioridade da notificação">
                {getFieldDecorator('notification.priority.number', {
                  initialValue: typeof notification.priority === 'object' ? notification.priority.number : notification.priority,
                  rules: [{ required: true, pattern: /^[0-9]*$/g, message: 'informe a prioridade (1 é a maior)' }],
                })(
                  <Input
                    addonBefore={getFieldDecorator('notification.priority.slot', { initialValue: typeof notification.priority === 'object' ? notification.priority.slot : 'Slot Normal' })(
                      <Select onChange={this.handleActionChange}>
                        <Select.Option value="Slot Normal">Slot Normal</Select.Option>
                        <Select.Option value="Slot Extra">Slot Extra</Select.Option>
                      </Select>
                    )}
                  />
                )}
              </FormItem>
            </Col>
            <Col span={6}>
              <FormItem label="Status">
                {getFieldDecorator('notification.status', { initialValue: notification.status })(
                  <Radio.Group options={[{ label: 'Ativo', value: 'active' }, { label: 'Inativo', value: 'inactive' }]} />
                )}
              </FormItem>
            </Col>
          </Row>
          {notification.screens.map((screen, index) => (
            <NotificationFormScreen
              screen={screen}
              index={index}
              key={`screen_${index}`}
              screensOptions={screensOptions}
              removeScreen={this.removeScreen}
              getFieldDecorator={getFieldDecorator}
              setFieldsValue={setFieldsValue}
              handleActionChange={this.handleActionChange}
            >
              <FormItem label="Botões" className="no-margin" />
              {screen.buttons &&
                screen.buttons.map((button, index2) => (
                  <NotificationFormButton
                    button={button}
                    index={index2}
                    screenIndex={index}
                    screenType={screen.type || ''}
                    screensOptions={screensOptions}
                    key={`screen_${index} - button_${index2}`}
                    removeButton={this.removeButton}
                    getFieldDecorator={getFieldDecorator}
                    handleActionChange={this.handleActionChange}
                  />
                ))}
              <Button type="dashed" onClick={() => this.addButton(index)} className="add-button">
                <Icon type="plus" /> Adicionar botão
              </Button>
            </NotificationFormScreen>
          ))}
          <FormItem>
            <Button type="dashed" onClick={this.addScreen} className="button-block add-screen">
              <Icon type="plus" /> Adicionar tela
            </Button>
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" size="large" loading={this.state.buttonLoading}>
              Salvar
            </Button>
          </FormItem>
        </StyledNotificationForm>
      </Form>
    );
  }
}

NotificationForm.propTypes = {
  form: PropTypes.object.isRequired,
  notification: PropTypes.object.isRequired,
  updateNotificationProps: PropTypes.func.isRequired,
  saveNotification: PropTypes.func.isRequired,
};

const mapStateToProps = store => ({
  notification: store.MainStore.notification,
});

const mapDispatchToProps = dispatch => ({
  updateNotificationProps: bindActionCreators(MainActions.updateNotificationProps, dispatch),
  saveNotification: bindActionCreators(MainActions.saveNotification, dispatch),
});

const mapPropsToFields = props => ({
  notification: Form.createFormField({ ...props.notification, value: props.notification.value }),
});

const validateMessages = () => ({
  required: 'Campo obrigatório',
  whitespace: 'Não pode ficar vazio',
  types: {
    regexp: 'Deve obedecer à regex: %s',
    url: 'Deve ser uma URL válida',
  },
  pattern: {
    mismatch: '%s value %s does not match pattern %s',
  },
  clone() {
    const cloned = JSON.parse(JSON.stringify(this));
    cloned.clone = this.clone;
    return cloned;
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Form.create({ mapPropsToFields, validateMessages })(NotificationForm));

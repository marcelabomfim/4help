import PropTypes from 'prop-types';
import { Component } from 'react';
import { Form, Input, Button, Radio, Checkbox, Row, Col, Icon, message } from 'components/_base/ANTD';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MainActions } from 'store/redux';

import StyledCategoryForm from './CategoryForm.styles';
import CategoryFormScreen from './CategoryFormScreen';
import CategoryFormButton from './CategoryFormButton';
import { Router } from '../../routes';

const buttonSkeleton = { style: '', text: '', action: '', actionValue: '', update: false, updateValue: '' };
const screenSkeleton = { internal_identification: '', title: '', text: '', buttons: [buttonSkeleton] };

class CategoryForm extends Component {
  state = {
    buttonLoading: false,
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll(err => {
      if (err) {
        message.error('Por favor preencha corretamente os campos indicados');
        return false;
      }

      if (this.state.buttonLoading) return false;

      const { category } = this.props;
      const hideLoading = message.loading(`Salvando registro`, 0);
      this.setState({ buttonLoading: true });

      this.props.saveCategory(category).then(() => {
        hideLoading();
        this.setState({ buttonLoading: false });
        setTimeout(() => {
          message.success(`Registro salvo com sucesso`);
          Router.pushRoute('/categorias');
        }, 600);
      });

      return false;
    });
  };

  handleChange = (e = false) => {
    if (e) e.preventDefault();
    const category = { ...this.props.category, ...this.props.form.getFieldValue('category') };
    this.props.updateCategoryProps(category);
  };

  addScreen = () => {
    const { category } = this.props;
    category.screens.push(screenSkeleton);

    this.props.updateCategoryProps(category);
    this.props.form.setFieldsValue({ category });
  };

  removeScreen = index => {
    const { category } = this.props;
    category.screens = category.screens.filter((screen, i) => i !== index);

    this.props.updateCategoryProps(category);
    this.props.form.setFieldsValue({ category });
  };

  addButton = screenIndex => {
    const { category } = this.props;
    const buttons = category.screens[screenIndex].buttons || [];
    buttons.push(buttonSkeleton);
    category.screens[screenIndex].buttons = buttons;

    this.props.updateCategoryProps(category);
    this.props.form.setFieldsValue({ category });
  };

  removeButton = (screenIndex, index) => {
    const { category } = this.props;
    category.screens[screenIndex].buttons = category.screens[screenIndex].buttons.filter((screen, i) => i !== index);

    this.props.updateCategoryProps(category);
    this.props.form.setFieldsValue({ category });
  };

  handleActionChange = () => {
    setTimeout(() => this.handleChange(), 1);
  };

  render() {
    const FormItem = Form.Item;
    const { getFieldDecorator, setFieldsValue } = this.props.form;
    const { category } = this.props;
    const screensOptions = category.screens.map((s, i) => ({ key: i, label: s.internal_identification })).filter(s => s.label !== '');

    return (
      <Form onSubmit={this.handleSubmit} onChange={this.handleChange}>
        <StyledCategoryForm>
          <Row gutter={24}>
            <Col span={12}>
              <FormItem label="Título da categoria">
                {getFieldDecorator('category.title', { initialValue: category.title, rules: [{ required: true, message: 'campo obrigatório' }] })(<Input />)}
              </FormItem>
            </Col>
            <Col span={6}>
              <FormItem label="Status">
                {getFieldDecorator('category.status', { initialValue: category.status })(<Radio.Group options={[{ label: 'Ativo', value: 'active' }, { label: 'Inativo', value: 'inactive' }]} />)}
              </FormItem>
            </Col>
            <Col span={6}>
              <FormItem label="Pedidos nos status">
                {getFieldDecorator('category.order_status', { initialValue: category.order_status })(
                  <Checkbox.Group options={[{ label: 'Pré-aprovação', value: 'Pré-aprovação' }, { label: 'Pós-aprovação', value: 'Pós-aprovação' }]} />
                )}
              </FormItem>
            </Col>
          </Row>
          {category.screens.map((screen, index) => (
            <CategoryFormScreen
              screen={screen}
              index={index}
              key={`screen_${index}`}
              screensOptions={screensOptions}
              removeScreen={this.removeScreen}
              getFieldDecorator={getFieldDecorator}
              setFieldsValue={setFieldsValue}
              handleActionChange={this.handleActionChange}
            >
              <FormItem label="Botões" className="no-margin" />
              {screen.buttons &&
                screen.buttons.map((button, index2) => (
                  <CategoryFormButton
                    button={button}
                    index={index2}
                    screenIndex={index}
                    screenType={screen.type || ''}
                    screensOptions={screensOptions}
                    key={`screen_${index} - button_${index2}`}
                    removeButton={this.removeButton}
                    getFieldDecorator={getFieldDecorator}
                    handleActionChange={this.handleActionChange}
                  />
                ))}
              <Button type="dashed" onClick={() => this.addButton(index)} className="add-button">
                <Icon type="plus" /> Adicionar botão
              </Button>
            </CategoryFormScreen>
          ))}
          <FormItem>
            <Button type="dashed" onClick={this.addScreen} className="button-block add-screen">
              <Icon type="plus" /> Adicionar tela
            </Button>
          </FormItem>
          <FormItem>
            <Button type="primary" htmlType="submit" size="large" loading={this.state.buttonLoading}>
              Salvar
            </Button>
          </FormItem>
        </StyledCategoryForm>
      </Form>
    );
  }
}

CategoryForm.propTypes = {
  form: PropTypes.object.isRequired,
  category: PropTypes.object.isRequired,
  updateCategoryProps: PropTypes.func.isRequired,
  saveCategory: PropTypes.func.isRequired,
};

const mapStateToProps = store => ({
  category: store.MainStore.category,
});

const mapDispatchToProps = dispatch => ({
  updateCategoryProps: bindActionCreators(MainActions.updateCategoryProps, dispatch),
  saveCategory: bindActionCreators(MainActions.saveCategory, dispatch),
});

const mapPropsToFields = props => ({
  category: Form.createFormField({ ...props.category, value: props.category.value }),
});

const validateMessages = () => ({
  required: 'Campo obrigatório',
  whitespace: 'Não pode ficar vazio',
  types: {
    regexp: 'Deve obedecer à regex: %s',
    url: 'Deve ser uma URL válida',
  },
  pattern: {
    mismatch: '%s value %s does not match pattern %s',
  },
  clone() {
    const cloned = JSON.parse(JSON.stringify(this));
    cloned.clone = this.clone;
    return cloned;
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Form.create({ mapPropsToFields, validateMessages })(CategoryForm));

import styled from 'styled-components';
import theme from 'theme/_variables';

export default styled.div`
  .screen-item {
    position: relative;
    margin-bottom: ${theme.spacing.small};
    background: ${theme.colors.white};
    border-color: ${theme.colors.primary};

    legend {
      color: ${theme.colors.primary};
    }

    &__delete {
      position: absolute;
      right: -${theme.spacing.small};
      top: ${theme.spacing.xsmall};
      font-size: 20px;
    }
  }

  .add-button {
    width: calc(50% - ${theme.spacing.small});

    &:nth-child(odd) {
      margin-left: ${theme.spacing.small};
    }
  }

  .button-item {
    position: relative;
    margin-bottom: ${theme.spacing.small};

    &:nth-child(even) {
      padding-right: ${theme.spacing.small};
      clear: both;
    }

    &:nth-child(odd) {
      padding-left: ${theme.spacing.small};
    }

    &__delete {
      position: absolute;
      right: -${theme.spacing.small};
      top: -${theme.spacing.xsmall};
    }

    .ant-col-8 {
      padding-right: ${theme.spacing.xsmall};
    }
  }

  .query-item {
    &:nth-child(odd) {
      padding-left: ${theme.spacing.small};
    }

    &:nth-child(even) {
      padding-right: ${theme.spacing.small};
    }

    .ant-col-8 {
      padding-right: ${theme.spacing.xsmall};
    }
  }
`;

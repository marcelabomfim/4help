import PropTypes from 'prop-types';
import { Alert, Form, Input, Radio, Select, Button, Row, Col } from 'components/_base/ANTD';
import { TextEditor } from 'components/_base/TextEditor';
import { queryTicketInfo, textParamsInfo } from 'components/TicketsInfo';

const CategoryFormScreen = ({ children, screen, index, screensOptions, removeScreen, getFieldDecorator, handleActionChange, setFieldsValue }) => {
  const FormItem = Form.Item;

  const handleClick = e => {
    e.preventDefault();
    e.stopPropagation();
    removeScreen(index);
  };

  const fieldPrefix = `category.screens[${index}]`;

  const handleFieldChange = (field, value) => {
    setFieldsValue({ [`${fieldPrefix}.${field}`]: value });
    handleActionChange();
  };

  return (
    <fieldset className="screen-item">
      <Button className="screen-item__delete" type="danger" shape="circle" icon="close" title="Remover tela" onClick={handleClick} />
      <legend>
        Tela {index} - {screen.internal_identification}
      </legend>
      <Row gutter={24}>
        <Col span={12}>
          <FormItem label="Identificação interna">
            {getFieldDecorator(`${fieldPrefix}.internal_identification`, { initialValue: screen.internal_identification, rules: [{ required: true, message: 'campo obrigatório' }] })(<Input />)}
          </FormItem>
        </Col>
        <Col span={12}>
          <FormItem label="Tipo">
            {getFieldDecorator(`${fieldPrefix}.type`, { initialValue: screen.type })(
              <Radio.Group options={[{ label: 'Exibir conteúdo', value: 'content' }, { label: 'Consultar zendesk', value: 'zendesk' }]} />
            )}
          </FormItem>
        </Col>
      </Row>

      <Row style={{ display: screen.type === 'content' ? 'block' : 'none' }}>
        <FormItem label="Título">{getFieldDecorator(`${fieldPrefix}.title`, { initialValue: screen.title })(<Input />)}</FormItem>
        <FormItem
          label={
            <span>
              Texto <Button shape="circle" size="small" icon="question" onClick={textParamsInfo} />
            </span>
          }
        >
          {getFieldDecorator(`${fieldPrefix}.text`, { initialValue: screen.text })(<TextEditor onChange={value => handleFieldChange('text', value)} />)}
        </FormItem>
        <div>{children}</div>
      </Row>

      <Row style={{ display: screen.type === 'zendesk' ? 'block' : 'none' }}>
        <FormItem
          label={
            <span>
              Regra da consulta <Button shape="circle" size="small" icon="question" onClick={queryTicketInfo} />
            </span>
          }
        >
          {getFieldDecorator(`${fieldPrefix}.zendeskQuery`, {
            initialValue: screen.zendeskQuery,
            rules: [{ required: screen.type === 'zendesk', pattern: /(&[^=?&]+=[^&?=])+/g, message: 'informe os parâmetros para montar a url' }],
          })(<Input addonBefore="http://...tickets?order=999" />)}
        </FormItem>
        <Row gutter={24}>
          <Col span={12} className="query-item">
            <Alert
              type="success"
              message={
                <div>
                  Se a consulta encontrar ticket:
                  <Row>
                    <Col span={8}>
                      <FormItem className="no-margin">
                        {getFieldDecorator(`${fieldPrefix}.zendeskFoundAction`, {
                          initialValue: screen.zendeskFoundAction !== '' ? screen.zendeskFoundAction : undefined,
                          rules: [{ required: screen.type === 'zendesk', message: 'campo obrigatório' }],
                        })(
                          <Select placeholder="Ação" onChange={handleActionChange}>
                            <Select.Option value="show_screen">Exibir tela</Select.Option>
                            <Select.Option value="link">Abrir link</Select.Option>
                            <Select.Option value="form">Abrir formulário</Select.Option>
                          </Select>
                        )}
                      </FormItem>
                    </Col>
                    <Col span={16}>
                      {screen.zendeskFoundAction === 'show_screen' && (
                        <FormItem className="no-margin">
                          {getFieldDecorator(`${fieldPrefix}.zendeskFoundActionValue`, {
                            initialValue: screen.zendeskFoundActionValue !== '' ? screen.zendeskFoundActionValue : undefined,
                            rules: [{ required: screen.type === 'zendesk', message: 'campo obrigatório' }],
                          })(
                            <Select placeholder="Escolha uma tela" onChange={handleActionChange}>
                              {screensOptions.map(s => (
                                <Select.Option value={s.key} key={s.key}>
                                  {s.key} - {s.label}
                                </Select.Option>
                              ))}
                            </Select>
                          )}
                        </FormItem>
                      )}
                      {screen.zendeskFoundAction === 'link' && (
                        <FormItem className="no-margin">
                          {getFieldDecorator(`${fieldPrefix}.zendeskFoundActionValue`, {
                            initialValue: screen.zendeskFoundActionValue,
                            rules: [{ required: screen.type === 'zendesk', type: 'url', message: 'informe uma url válida' }],
                          })(<Input placeholder="Informe o link" />)}
                        </FormItem>
                      )}
                      {screen.zendeskFoundAction === 'form' && (
                        <FormItem className="no-margin">
                          {getFieldDecorator(`${fieldPrefix}.zendeskFoundActionValue`, {
                            initialValue: screen.zendeskFoundActionValue,
                            rules: [{ required: screen.type === 'zendesk', pattern: /\w+,?/g, message: 'informe os campos separando por vírgula' }],
                          })(<Input placeholder="Informe os campos visíveis" />)}
                        </FormItem>
                      )}
                      {(screen.zendeskFoundAction === '' || !screen.zendeskFoundAction) && (
                        <FormItem className="no-margin">
                          {getFieldDecorator(`${fieldPrefix}.zendeskFoundActionValue`, {
                            initialValue: screen.zendeskFoundActionValue,
                            rules: [{ required: screen.type === 'zendesk', message: 'campo obrigatório' }],
                          })(<Input placeholder="Valor da ação" />)}
                        </FormItem>
                      )}
                    </Col>
                  </Row>
                </div>
              }
            />
          </Col>
          <Col span={12} className="query-item">
            <Alert
              type="error"
              message={
                <div>
                  Se a consulta não encontrar ticket:
                  <Row>
                    <Col span={8}>
                      <FormItem className="no-margin">
                        {getFieldDecorator(`${fieldPrefix}.zendeskNotFoundAction`, {
                          initialValue: screen.zendeskNotFoundAction !== '' ? screen.zendeskNotFoundAction : undefined,
                          rules: [{ required: screen.type === 'zendesk', message: 'campo obrigatório' }],
                        })(
                          <Select placeholder="Ação" onChange={handleActionChange}>
                            <Select.Option value="show_screen">Exibir tela</Select.Option>
                            <Select.Option value="link">Abrir link</Select.Option>
                            <Select.Option value="form">Abrir formulário</Select.Option>
                          </Select>
                        )}
                      </FormItem>
                    </Col>
                    <Col span={16}>
                      {screen.zendeskNotFoundAction === 'show_screen' && (
                        <FormItem className="no-margin">
                          {getFieldDecorator(`${fieldPrefix}.zendeskNotFoundActionValue`, {
                            initialValue: screen.zendeskNotFoundActionValue !== '' ? screen.zendeskNotFoundActionValue : undefined,
                            rules: [{ required: screen.type === 'zendesk', message: 'campo obrigatório' }],
                          })(
                            <Select placeholder="Escolha uma tela" onChange={handleActionChange}>
                              {screensOptions.map(s => (
                                <Select.Option value={s.key} key={s.key}>
                                  {s.key} - {s.label}
                                </Select.Option>
                              ))}
                            </Select>
                          )}
                        </FormItem>
                      )}
                      {screen.zendeskNotFoundAction === 'link' && (
                        <FormItem className="no-margin">
                          {getFieldDecorator(`${fieldPrefix}.zendeskNotFoundActionValue`, {
                            initialValue: screen.zendeskNotFoundActionValue,
                            rules: [{ required: screen.type === 'zendesk', type: 'url', message: 'informe uma url válida' }],
                          })(<Input placeholder="Informe o link" />)}
                        </FormItem>
                      )}
                      {screen.zendeskNotFoundAction === 'form' && (
                        <FormItem className="no-margin">
                          {getFieldDecorator(`${fieldPrefix}.zendeskNotFoundActionValue`, {
                            initialValue: screen.zendeskNotFoundActionValue,
                            rules: [{ required: screen.type === 'zendesk', pattern: /\w+,?/g, message: 'informe os campos separando por vírgula' }],
                          })(<Input placeholder="Informe os campos visíveis" />)}
                        </FormItem>
                      )}
                      {(screen.zendeskNotFoundAction === '' || !screen.zendeskNotFoundAction) && (
                        <FormItem className="no-margin">
                          {getFieldDecorator(`${fieldPrefix}.zendeskNotFoundActionValue`, {
                            initialValue: screen.zendeskNotFoundActionValue,
                            rules: [{ required: screen.type === 'zendesk', message: 'campo obrigatório' }],
                          })(<Input placeholder="Valor da ação" />)}
                        </FormItem>
                      )}
                    </Col>
                  </Row>
                </div>
              }
            />
          </Col>
        </Row>
      </Row>
    </fieldset>
  );
};

CategoryFormScreen.propTypes = {
  children: PropTypes.node.isRequired,
  screen: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  screensOptions: PropTypes.array.isRequired,
  removeScreen: PropTypes.func.isRequired,
  getFieldDecorator: PropTypes.func.isRequired,
  handleActionChange: PropTypes.func.isRequired,
  setFieldsValue: PropTypes.func.isRequired,
};

export default CategoryFormScreen;

import PropTypes from 'prop-types';
import { Table, Button, Popconfirm, message } from 'components/_base/ANTD';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MainActions } from 'store/redux';

import StyledNotificationsList from './NotificationsList.styles';
import { Link } from '../../routes';

const NotificationsList = ({ notifications, deleteNotification }) => {
  const confirmDelete = id => {
    const hideLoading = message.loading(`Excluindo registro ${id}`, 0);
    deleteNotification(id).then(() => {
      hideLoading();
      setTimeout(() => message.success(`Registro ${id} excluído`), 600);
    });
  };

  const getFirstRule = notification => {
    const q = notification.screens[0].query || false;

    return Object.keys(q)
      .map(key => `${key}=${q[key]}`)
      .join(' & ');
  };

  const getPriority = notification => {
    const slot = typeof notification.priority === 'object' ? notification.priority.slot : 'Slot Normal';
    const number = typeof notification.priority === 'object' ? notification.priority.number : notification.priority;

    return `${slot} - ${number}`;
  };

  const dataSource = notifications.map(notification => ({
    key: notification.title,
    id: notification.id,
    title: notification.title,
    priority: getPriority(notification),
    rule: getFirstRule(notification),
    screensQty: notification.screens && notification.screens.length,
    status: notification.status,
  }));

  const columns = [
    {
      title: 'Prioridade',
      dataIndex: 'priority',
      key: 'priority',
    },
    {
      title: 'Título',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Regra Principal',
      dataIndex: 'rule',
      key: 'rule',
    },
    {
      title: 'Qtd. Telas',
      dataIndex: 'screensQty',
      key: 'screensQty',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: text => (text === 'active' ? 'Ativa' : 'Inativo'),
    },
    {
      title: 'Ação',
      key: 'action',
      align: 'center',
      width: 250,
      render: (text, record) => (
        <span>
          <Link route={`/notificacoes/editar/${record.id}`}>
            <Button icon="edit">Editar</Button>
          </Link>
          <Popconfirm title="Tem certeza que deseja excluir este registro?" onConfirm={() => confirmDelete(record.id)} okText="Sim" cancelText="Não" placement="left">
            <Button type="danger" icon="delete">
              Excluir
            </Button>
          </Popconfirm>
        </span>
      ),
    },
  ];

  return (
    <StyledNotificationsList>
      <Table dataSource={dataSource} columns={columns} bordered />
    </StyledNotificationsList>
  );
};

NotificationsList.propTypes = {
  notifications: PropTypes.array.isRequired,
  deleteNotification: PropTypes.func.isRequired,
};

const mapStateToProps = store => ({
  notifications: store.MainStore.notifications,
});

const mapDispatchToProps = dispatch => ({
  deleteNotification: bindActionCreators(MainActions.deleteNotification, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsList);

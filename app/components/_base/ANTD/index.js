import { AlertCss } from 'antd/lib/alert/style/index.css';
import { BreadcrumbCss } from 'antd/lib/breadcrumb/style/index.css';
import { ButtonCss } from 'antd/lib/button/style/index.css';
import { CheckboxCss } from 'antd/lib/checkbox/style/index.css';
import { ColCss } from 'antd/lib/grid/style/index.css';
import { FormCss } from 'antd/lib/form/style/index.css';
import { IconCss } from 'antd/lib/style/index.css';
import { InputCss } from 'antd/lib/input/style/index.css';
import { MenuCss } from 'antd/lib/menu/style/index.css';
import { ModalCss } from 'antd/lib/modal/style/index.css';
import { messageCss } from 'antd/lib/message/style/index.css';
import { PaginationCss } from 'antd/lib/pagination/style/index.css';
import { PopconfirmCss } from 'antd/lib/popover/style/index.css';
import { RadioCss } from 'antd/lib/radio/style/index.css';
import { SelectCss } from 'antd/lib/select/style/index.css';
import { TableCss } from 'antd/lib/table/style/index.css';
import { TooltipCSS } from 'antd/lib/tooltip/style/index.css';

export { default as Alert } from 'antd/lib/alert';
export { default as Breadcrumb } from 'antd/lib/breadcrumb';
export { default as Button } from 'antd/lib/button';
export { default as Checkbox } from 'antd/lib/checkbox';
export { default as Col } from 'antd/lib/col';
export { default as Form } from 'antd/lib/form';
export { default as Icon } from 'antd/lib/icon';
export { default as Input } from 'antd/lib/input';
export { default as Menu } from 'antd/lib/menu';
export { default as Modal } from 'antd/lib/modal';
export { default as message } from 'antd/lib/message';
export { default as Popconfirm } from 'antd/lib/popconfirm';
export { default as Radio } from 'antd/lib/radio';
export { default as Row } from 'antd/lib/row';
export { default as Select } from 'antd/lib/select';
export { default as Table } from 'antd/lib/table';
export { default as Tooltip } from 'antd/lib/tooltip';

export const antdCSS = `
  ${AlertCss}
  ${BreadcrumbCss}
  ${ButtonCss}
  ${CheckboxCss}
  ${ColCss}
  ${FormCss}
  ${IconCss}
  ${InputCss}
  ${MenuCss}
  ${ModalCss}
  ${messageCss}
  ${PaginationCss}
  ${PopconfirmCss}
  ${RadioCss}
  ${SelectCss}
  ${TableCss}
  ${TooltipCSS}
`;

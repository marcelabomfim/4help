/* eslint global-require: 0, react/no-did-mount-set-state: 0 */
import { Component } from 'react';
import PropTypes from 'prop-types';

import StyledTextEditor from './TextEditor.styles';

let Quill;

class TextEditor extends Component {
  state = {
    useQuill: false,
  };

  componentDidMount() {
    Quill = require('react-quill');

    this.setState({
      useQuill: true,
    });
  }

  render() {
    const modules = {
      toolbar: [[{ size: [] }, { align: [] }, { color: ['#424242', '#0488d2', '#ffca29', '#4caf50', '#e53936', '#757575', '#ffffff'] }, 'bold', 'italic', 'underline', 'link'], ['clean']],
    };

    return <StyledTextEditor>{this.state.useQuill && <Quill modules={modules} value={this.props.value} onChange={this.props.onChange} />}</StyledTextEditor>;
  }
}

TextEditor.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

TextEditor.defaultProps = {
  value: '',
};

export default TextEditor;

import styled from 'styled-components';
import theme from 'theme/_variables';

export default styled.div`
  .ql-toolbar.ql-snow {
    border: 1px solid ${theme.colors.gray};
    padding: 0 8px;
    border-radius: 4px 4px 0 0;
    background: ${theme.colors.grayMedium};

    & + .ql-container.ql-snow {
      border-radius: 0 0 4px 4px;
    }

    .ql-picker {
      line-height: 1;
    }
  }

  .ql-container.ql-snow {
    border: 1px solid ${theme.colors.gray};
  }

  .ql-editor {
    font-size: 14px;

    .ql-size-huge {
      font-size: 20px;
    }

    .ql-size-large {
      font-size: 17px;
    }

    .ql-size-small {
      font-size: 11px;
    }
  }
`;

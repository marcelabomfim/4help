import NextHead from 'next/head';
import PropTypes from 'prop-types';

const Head = props => (
  <NextHead>
    <meta charSet="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta httpEquiv="Content-Language" content="pt-br" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, width=device-width" />
    <meta name="application-name" content="LojasKD.com.br" />
    <meta name="msapplication-TileColor" content="#ffe01a" />
    <meta name="theme-color" content="#ffe01a" />
    <link rel="icon" type="image/png" href="/static/favicon.png" />
    <title>{props.title}</title>
  </NextHead>
);

Head.propTypes = {
  title: PropTypes.string,
};

Head.defaultProps = {
  title: 'LojasKD 4Help',
};

export default Head;

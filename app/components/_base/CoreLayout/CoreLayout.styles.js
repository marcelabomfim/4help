import styled from 'styled-components';
import theme from 'theme/_variables';

export default styled.div`
  background: ${theme.colors.background};
  display: flex;
  min-height: 100vh;

  aside {
    width: 280px;
    min-height: 100%;
    background: ${theme.colors.backgroundDark};
    box-shadow: 5px 0 30px -20px ${theme.colors.black};

    h1 {
      line-height: 0.8;
      padding: 24px 24px 24px 68px;
      position: relative;
      color: ${theme.colors.white};

      img {
        position: absolute;
        left: 24px;
        top: 24px;
      }

      strong {
        font-size: 24px;
      }

      small {
        font-size: 14px;
      }
    }

    .menu {
      border-right: 0;
    }
  }

  main {
    width: 100%;
    padding: 24px 48px;

    .breadcrumb {
      margin: 16px 0;
    }

    &-children {
      /* ... */
    }
  }
`;

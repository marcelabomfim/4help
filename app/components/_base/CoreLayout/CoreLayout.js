import { Breadcrumb, Icon, Menu } from 'components/_base/ANTD';
import { initGA, logPageView } from 'helpers/analytics';
import PropTypes from 'prop-types';
import { Component } from 'react';
import { CookiesProvider } from 'react-cookie';
import NProgress from 'nprogress';

import favicon from 'static/favicon.png';
import Head from './CoreLayoutHead';
import Styled from './CoreLayout.styles';

import { Link, Router } from '../../../routes';

Router.onRouteChangeStart = () => NProgress.start();
Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChangeError = () => NProgress.done();

class CoreLayout extends Component {
  componentDidMount() {
    if (!window.GA_INITIALIZED) {
      initGA();
      window.GA_INITIALIZED = true;
    }

    logPageView();
  }

  render() {
    const { SubMenu } = Menu;
    const { children, page, subPage, activeMenu, activeSubmenu } = this.props;

    return (
      <CookiesProvider>
        <Head />
        <Styled>
          <aside>
            <Link route="/">
              <a>
                <h1>
                  <img src={favicon} alt="LojasKD.com.br" />
                  <strong>4Help</strong>
                  <br />
                  <small>Conteúdos</small>
                </h1>
              </a>
            </Link>
            <Menu className="menu" mode="inline" defaultSelectedKeys={[activeMenu]} defaultOpenKeys={[activeSubmenu]} theme="dark">
              <Menu.Item key="1">
                <Link route="/">
                  <a>
                    <Icon type="home" /> Inínio
                  </a>
                </Link>
              </Menu.Item>
              <SubMenu
                key="2"
                title={
                  <span>
                    <Icon type="user" />Auto Atendimento
                  </span>
                }
              >
                <Menu.Item key="21">
                  <Link route="/categorias">
                    <a>Listar</a>
                  </Link>
                </Menu.Item>
                <Menu.Item key="22">
                  <Link route="/categorias/adicionar">
                    <a>Adicionar</a>
                  </Link>
                </Menu.Item>
              </SubMenu>
              <SubMenu
                key="3"
                title={
                  <span>
                    <Icon type="exclamation-circle-o" />Notificações
                  </span>
                }
              >
                <Menu.Item key="23">
                  <Link route="/notificacoes">
                    <a>Listar</a>
                  </Link>
                </Menu.Item>
                <Menu.Item key="24">
                  <Link route="/notificacoes/adicionar">
                    <a>Adicionar</a>
                  </Link>
                </Menu.Item>
              </SubMenu>
            </Menu>
          </aside>
          <main>
            <div>
              <Breadcrumb className="breadcrumb">
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>{page}</Breadcrumb.Item>
                <Breadcrumb.Item>{subPage}</Breadcrumb.Item>
              </Breadcrumb>
              <div className="main-children">{children}</div>
            </div>
          </main>
        </Styled>
      </CookiesProvider>
    );
  }
}

CoreLayout.propTypes = {
  children: PropTypes.node.isRequired,
  page: PropTypes.string,
  subPage: PropTypes.string,
  activeMenu: PropTypes.string,
  activeSubmenu: PropTypes.string,
};

CoreLayout.defaultProps = {
  page: '',
  subPage: '',
  activeMenu: '1',
  activeSubmenu: '',
};

export default CoreLayout;

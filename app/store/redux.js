import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

import requests from './requests';

// INITIAL STATE
const MainInitialState = {
  categories: [],
  category: {},
  notifications: [],
  notification: {},
};

// ACTIONS
export const MainActions = {
  // Categorie sections
  getCategories: () => dispatch =>
    requests.categories.all().then(result => {
      dispatch({ type: 'GET_CATEGORIES', value: result });
    }),
  getCategory: categoryId => dispatch =>
    requests.categories.get(categoryId).then(result => {
      dispatch({ type: 'GET_CATEGORY', value: result });
    }),
  saveCategory: categoryData => () => {
    if (!categoryData.id || categoryData.id === '') {
      const newCategoryData = categoryData;
      delete newCategoryData.id;
      return requests.categories.create(newCategoryData);
    }
    return requests.categories.update(categoryData);
  },
  deleteCategory: categoryId => dispatch =>
    requests.categories.delete(categoryId).then(() => {
      dispatch({ type: 'DELETE_CATEGORY', value: categoryId });
    }),
  initialCategory: () => dispatch =>
    requests.categories.initial().then(result => {
      dispatch({ type: 'GET_CATEGORY', value: result });
    }),
  updateCategoryProps: category => dispatch => dispatch({ type: 'UPDATE_CATEGORY_PROPS', value: category }),
  // Notifications sections
  getNotifications: () => dispatch =>
    requests.notifications.all().then(result => {
      dispatch({ type: 'GET_NOTIFICATIONS', value: result });
    }),
  getNotification: notificationId => dispatch =>
    requests.notifications.get(notificationId).then(result => {
      dispatch({ type: 'GET_NOTIFICATION', value: result });
    }),
  saveNotification: notificationData => () => {
    if (!notificationData.id || notificationData.id === '') {
      const newNotificationData = notificationData;
      delete newNotificationData.id;
      return requests.notifications.create(newNotificationData);
    }
    return requests.notifications.update(notificationData);
  },
  deleteNotification: notificationId => dispatch =>
    requests.notifications.delete(notificationId).then(() => {
      dispatch({ type: 'DELETE_NOTIFICATION', value: notificationId });
    }),
  initialNotification: () => dispatch =>
    requests.notifications.initial().then(result => {
      dispatch({ type: 'GET_NOTIFICATION', value: result });
    }),
  updateNotificationProps: notification => dispatch => dispatch({ type: 'UPDATE_NOTIFICATION_PROPS', value: notification }),
};

// REDUCERS
export const MainReducer = (state = MainInitialState, action) => {
  switch (action.type) {
    // Catetories sections
    case 'GET_CATEGORIES':
      return { ...state, categories: action.value };
    case 'GET_CATEGORY':
      return { ...state, category: action.value };
    case 'DELETE_CATEGORY':
      return { ...state, categories: state.categories.filter(category => category.id !== action.value) };
    case 'UPDATE_CATEGORY_PROPS':
      return { ...state, category: action.value };
    // Notifications sections
    case 'GET_NOTIFICATIONS':
      return { ...state, notifications: action.value };
    case 'GET_NOTIFICATION':
      return { ...state, notification: action.value };
    case 'DELETE_NOTIFICATION':
      return { ...state, notifications: state.notifications.filter(notification => notification.id !== action.value) };
    case 'UPDATE_NOTIFICATION_PROPS':
      return { ...state, notification: action.value };
    default:
      return state;
  }
};

export const Reducers = combineReducers({
  MainStore: MainReducer,
});

export const initStore = initialState => createStore(Reducers, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)));

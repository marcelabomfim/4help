import staticContent from 'static/data/mockContent.json';
import fetch from 'helpers/fetch';
import { apiRoot as configApiRoot } from '../config';

let apiRoot = configApiRoot;
if (!apiRoot || typeof apiRoot === 'undefined' || apiRoot === 'undefined') {
  apiRoot = 'https://vlhkqzz1c1.execute-api.us-east-1.amazonaws.com/prod/forhelp';
}

const req = {
  get: url => fetch(`${apiRoot}${url}`),
  post: (url, body) => fetch(`${apiRoot}${url}`, { method: 'POST', body: JSON.stringify(body) }),
  put: (url, body) => fetch(`${apiRoot}${url}`, { method: 'PUT', body: JSON.stringify(body) }),
  del: url => fetch(`${apiRoot}${url}`, { method: 'DELETE' }),
};

const categories = {
  initial: async () => staticContent.category,
  all: async () => req.get('/selfservice').then(res => res.body.Items),
  get: async id => req.get(`/selfservice/${id}`).then(res => res.body.Items[0]),
  create: async data => req.post(`/selfservice`, data).then(res => res.status),
  update: async data => req.put(`/selfservice/${data.id}`, data).then(res => res.status),
  delete: async id => req.del(`/selfservice/${id}`).then(res => res.status),
};

const notifications = {
  initial: async () => staticContent.notification,
  all: async () => req.get('/notification').then(res => res.body.Items),
  get: async id => req.get(`/notification/${id}`).then(res => res.body.Items[0]),
  create: async data => req.post(`/notification`, data).then(res => res.status),
  update: async data => req.put(`/notification/${data.id}`, data).then(res => res.status),
  delete: async id => req.del(`/notification/${id}`).then(res => res.status),
};

export default {
  categories,
  notifications,
};

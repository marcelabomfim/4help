[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier) [![Cypress.io tests](https://img.shields.io/badge/cypress.io-tests-green.svg?style=flat-square)](https://cypress.io)

# 4 Help

The LKD frontend application to management content of 4help project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* [Docker](https://www.docker.com) at last version (Docker-ce 17.x)
* [Docker Compose](https://docs.docker.com/compose/) at least version (>1.16), for running containers locally
* [Start and running any LojasKD application](http://wiki.lojaskd.com.br/wiki/index.php/README_-_First_things_firt)

### Starting

Run the command `make up` then get the 4help ip, that will be printed, and access on the browser.

### Make Scripts

* `make up` build and run app
* `make rebuild` force recreate app with no cache
* `make ips` print app available IP's
* `make logs` app logs in realtime
* and [others](./Makefile.sh)

### NPM Scripts

* `yarn build` build a production version
* `yarn lint` check code style with [`eslint`](http://eslint.org/), [`airbnb`](https://github.com/airbnb/javascript/tree/master/react) react style guide and [`prettier`](https://prettier.io)
* `yarn lint:fix` check code style for _eslint_ errors and fix issues with _prettier_
* `yarn test` run acceptance tests with [cypress](https://www.cypress.io/) in headless mode
* `yarn test:open` open the _Cypress Test Runner_ in interactive mode
* `yarn create:component ComponentName` creates a component
* `yarn create:page PageName` creates a page

## Folder structure:

```
|- app/
|   |- pages/                // All pages file (index, about ..)
|   |   |- index.js
|
|   |- components/           // Reusable components
|   |   |- _base/            // Base views definitions (layout, head)
|   |   |   |- CoreLayout/
|   |   |   |   |- index.js  // Core layout
|   |   |   |   |- head.js   // Override head
|   |   |
|   |   |- Footer/
|   |   |- Header/
|   |   |   |- index.js      // Component logic
|   |   |   |- styles.css    // Specific component styles
|   |
|   |- static/               // Statis files (images, fonts ...)
|
|- tests/                    // Tests files
|   |- integration/
|   |   |- homepage_spec.js  // Integration test for home page
|
|- docker/                   // Docker config files, environments variables
|
|- tools/                    // Tools scripts (create page/component)
|
\- ...build and configuration files
```

> For a better _code standards_ we recommend to use [VSCode](https://code.visualstudio.com/) with [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) and [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) extensions.

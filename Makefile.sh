ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
$(eval $(ARGS):;@:)

MAKEFLAGS += --silent
DOCKER_COMPOSE = docker-compose -f docker/docker-compose.yml -f docker/docker-compose.override.yml

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Docker machine states
#############################
dc:
	$(DOCKER_COMPOSE) $(ARGS)

up:
	# cleaning cache for environment variables
	sudo rm -rf ./node_modules/.cache/babel-loader
	cat docker/environments/$(ARGS).dist > .env
	$(DOCKER_COMPOSE) up -d --force-recreate
	# $(DOCKER_COMPOSE) exec -u $$(id -u) 4help-node npm install
	echo ------------------------------------------
	make ips

rebuild:
	$(DOCKER_COMPOSE) stop
	$(DOCKER_COMPOSE) build --no-cache --pull
	make up $(ARGS)

logs:
	$(DOCKER_COMPOSE) logs --follow $(ARGS)

shell:
	$(DOCKER_COMPOSE) exec 4help /bin/sh

bash: shell

ips:
	$(DOCKER_COMPOSE) ps | sed -e '/Name/,+1d' | awk '{print $$1}' | while read f; do echo "$$f -> "$$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $$f); done


#############################
# Argument fix workaround
#############################
%:
@:

describe('Visit the Home Page', () => {
  it('assert that <title> is correct and welcome message is present', () => {
    cy.visit('/');
    cy.title().should('include', 'LojasKD');
    cy.contains('LojasKD Application');
  });
});

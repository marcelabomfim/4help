const fs = require('fs');
const path = require('path');

const pagesPath = 'app/pages/';

function catchErr(err) {
  if (err) {
    throw err;
  }
}

function writeJs(dirPath, dirName) {
  let str = `\
import { Component } from 'react';
import { CoreLayout } from 'components/_base/CoreLayout';

class ${dirName}Page extends Component {
  render() {
    return (
      <CoreLayout>
        <h1 className="title">Welcome to ${dirName} page!</h1>
      </CoreLayout>
    );
  }
}

export default ${dirName}Page;
`;

  str = Buffer.from(str);
  fs.writeFile(path.join(dirPath, `${dirName.toLowerCase()}.js`), str, catchErr);
}

module.exports = function createComponent(dirNames) {
  if (dirNames.length === 0) {
    throw Error('Please enter the name of the component');
  }

  const dirName = dirNames[0];
  const dirPath = path.join(pagesPath);

  writeJs(dirPath, dirName);
};

#!/usr/bin/env node
/* eslint no-console: 0 */

const program = require('commander');

const createComponent = require('./createComponent');
const createPage = require('./createPage');

function parseVal(val) {
  return val.split(',');
}

function log(type, name) {
  console.log(`you will create ${type} ${name}`);
}

(function() {
  /* eslint func-names: 0 */

  program
    .version('1.0.0')
    .option('-p, --page [name]', 'Create Page', parseVal)
    .option('-c, --component [name]', 'Create Component', parseVal)
    .parse(process.argv);

  if (program.page) {
    log('page', program.page);
    createPage(program.page);
  } else if (program.component) {
    log('component', program.component);
    createComponent(program.component);
  }
})();
